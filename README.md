# Yellow Jackal Test Project

Designs: [https://www.figma.com/file/TaedZstpICxaVrmndNnZBA/Yellow-Jackal-Test?node-id=0%3A1](https://www.figma.com/file/TaedZstpICxaVrmndNnZBA/Yellow-Jackal-Test?node-id=0%3A1)

## Goals

1. Create a test project in Angular using Bootstrap 5
2. Build out as many screens as possible with supporting libs you choose.
  1. Date picker
  2. Select
  3. Table with pagination
3. Match the designs as close as possible

## Deliverable

After the allotted time is up and you feel comfortable with the result, share a link to repository where the code is stored. Username to share with: [https://github.com/ryanbeymer](https://github.com/ryanbeymer) or [https://bitbucket.org/ryanbeymer](https://bitbucket.org/ryanbeymer).